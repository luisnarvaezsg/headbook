package com.hiberus.headbook.exception;

public class HeadBookException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -384596350086620430L;


	public HeadBookException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HeadBookException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public HeadBookException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public HeadBookException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
